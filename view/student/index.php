<?php
include_once '../include/header.php';

include_once('../../vendor/autoload.php');
  
$view_data = new App\admin\Product\Product();

$all_view_data = $view_data->view();


?>

<style>
  .product img{
    height: 200px !important;
  }

</style>
    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">All Product</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">



          <?php foreach( $all_view_data as $single_one ){ ?>

          <div class="col-md-3 col-sm-6 product">
            	<span class="thumbnail">
          			<img src="view/uploads/<?php echo $single_one['thumbnail'] ?>" alt="...">

          			<h4><?php echo $single_one['product_title'] ?></h4>

          			<div class="ratings">
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star-empty"></span>
                    </div>

          			<p><?php echo $single_one['product_description'] ?></p>

          			<hr class="line">
          			<div class="row">
          				<div class="col-md-6 col-sm-6">

          					<p class="price">৳<?php echo $single_one['product_price'] ?></p>

          				</div>
          				<div class="col-md-6 col-sm-6">
          				 <a href="view/student/view.php?id=<?php echo $single_one['id'] ?>" target="_blank" >	<button class="btn btn-info right" >Details</button></a>
          				</div>
          			</div>
    		      </span>
          </div>

          <?php } ?>

            <!-- END PRODUCTS -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>